<?php declare(strict_types=1);

return [
	// Enables verbose logging
	'Debug' => true,
	// Application namespace
	'Namespace' => 'TestApp',

	'Module' => [
		// Class name of the module class given the module {Name} and {Namespace}
		'ClassNamePattern' => '\{Namespace}\{Name}\{Name}Module',
	],

	'Route' => [
		// Controller and action for Route not found
		'NotFound' => ['module.notfound.controller', 'notFound'],
		// Controller and action for Route found, but not allow / permitted
		'NotAllowed' => ['module.notfound.controller', 'notAllowed'],
	],

	'Middleware' => [
		\Arrow\Middleware\ModuleLoader::class,
	],
	'Package' => [
		'arrowphp/cli' => [
			'Path' => [
				'Base' => __DIR__ . '/../..',
				'Self' => __DIR__ . '/../../vendor/arrowphp/arrow',
			],
			'Namespace' => 'Arrow\\CLI',
		]
	],
];
