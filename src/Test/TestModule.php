<?php

declare(strict_types=1);

namespace Arrow\CLI\Test;

use Arrow\ApplicationBuilderOptions;
use Arrow\Builder\ModulesBuilderOptions;
use Arrow\Interface\Module;

class TestModule implements Module {
	public function register(ApplicationBuilderOptions $options): void {
		$options->modules(function (ModulesBuilderOptions $options) {
			$options->registerCliCommand(TestRun::class);
			$options->registerCliCommand(TestCoverage::class);
		});
	}
}
