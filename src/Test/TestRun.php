<?php

declare(strict_types=1);

namespace Arrow\CLI\Test;

use Arrow\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestRun extends Command {

	private string $passthroughCommand = 'phpunit --testdox --colors=always';

	protected function configure()
	{
		$this->setName('test:run');
		$this->setDescription('Run PHPUnit tests');
		$this->setHelp(str_replace('%passthroughCommand%', $this->passthroughCommand, <<<'EOF'
The <info>%command.name%</info> command runs your phpunit test suite and outputs an HTML coverage report:

  <info>%command.full_name%</info>

This command that will be executed:

  <info>%passthroughCommand%</info>

EOF));
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		$command = Application::$VENDOR_DIR . '/bin/' .$this->passthroughCommand;

		$output->writeln($command);

		passthru($command, $resultCode);

		return $resultCode;
	}
}
