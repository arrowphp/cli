<?php

declare(strict_types=1);

namespace Arrow\CLI\Lint;

use Arrow\ApplicationBuilderOptions;
use Arrow\Builder\ModulesBuilderOptions;
use Arrow\Interface\Module;

class LintModule implements Module {
	public function register(ApplicationBuilderOptions $options): void {
		$options->modules(function (ModulesBuilderOptions $options) {
			$options->registerCliCommand(LintRun::class);
			$options->registerCliCommand(LintFix::class);
		});
	}
}
