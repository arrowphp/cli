<?php

declare(strict_types=1);

namespace Arrow\CLI\Lint;

use Arrow\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LintRun extends Command {

	private string $passthroughCommand = 'phpcs -p --standard=$standard src/ test/ test-app/src/';

	protected function configure()
	{
		$this->setName('lint:run');
		$this->setDescription('Run phpcs Linting');
		$this->setDefinition([
			new InputOption('standard', 's', InputOption::VALUE_REQUIRED, 'The standard to run (ArrowPHP, PSR2)', 'ArrowPHP'),
		]);
		$this->setHelp(str_replace('%passthroughCommand%', $this->passthroughCommand, <<<'EOF'
The <info>%command.name%</info> command runs phpcs Linting:

  <info>%command.full_name%</info>

This command that will be executed:

  <info>%passthroughCommand%</info>

EOF));
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		$standard = $input->getOption('standard');

		if ($standard === 'ArrowPHP') {
			$standard = Application::$VENDOR_DIR . '/arrowphp/arrow/lint';
		}

		$passthroughCommand = str_replace('$standard', $standard, $this->passthroughCommand);

		$command = Application::$VENDOR_DIR . '/bin/' . $passthroughCommand;

		$output->writeln($command);

		passthru($command, $resultCode);

		return $resultCode;
	}
}
