# Arrow

[![Software License][ico-license]](LICENSE.md)
<!-- [![Build Status][ico-buildstatus]][link-pipelines] -->
<!-- [![Latest Version on Packagist][ico-version]][link-packagist] -->
<!-- [![Total Downloads][ico-downloads]][link-downloads] -->
<!-- [![Coverage Status][ico-scrutinizer]][link-scrutinizer] -->
<!-- [![Quality Score][ico-code-quality]][link-code-quality] -->

This is the Arrow CLI. It adds CLI capability into to any Arrow powered application.

## Install

Via project composer.json

``` json
  "require": {
    "arrowphp/arrow": "@dev",
    "arrowphp/cli": "@dev"
  }
```

## Usage

``` bash
$ vendor/bin/arrow-cli
```

### Implementation

Now available in the module class is a `registerCLI` method. This method will register the defined command and make it available to be used via the `arrow-cli`.

``` php
    public function registerCLI(\Arrow\CLI\CLI $cli, \League\Container\Container $container, \Arrow\Config $config): void
    {
        // Call an anonymouse function directly
        $cli->register('<module>:<tag>', '<description>', function () {
            // Do work
        });
        // or call a controller / controller action.
        $cli->register('<module>:<tag>', '<description>', '<container-name>');
    }

    public function register(Container $container, Config $config)
    {
        $container->add('<container-name>', <Module>\<ClassName>::class)
            ->withArgument($container)
            ->withArgument($config);
    }
```


## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please contact [Chris Pennycuick][link-author] directly instead of using the issue tracker.

## Credits

- [Chris Pennycuick][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

<!-- [ico-version]: https://img.shields.io/packagist/v/arrowphp/arrow.svg?style=flat-square -->
<!-- [ico-downloads]: https://img.shields.io/packagist/dt/arrowphp/arrow.svg?style=flat-square -->
<!-- [ico-buildstatus]: https://gitlab.com/arrowphp/arrow/badges/master/build.svg -->
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
<!-- [ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/:vendor/:package_name.svg?style=flat-square -->
<!-- [ico-code-quality]: https://img.shields.io/scrutinizer/g/:vendor/:package_name.svg?style=flat-square -->

<!-- [link-packagist]: https://packagist.org/packages/arrowphp/arrow -->
<!-- [link-downloads]: https://packagist.org/packages/arrowphp/arrow -->
[link-author]: https://gitlab.com/christopher.pennycuick
[link-contributors]: https://gitlab.com/arrowphp/cli/graphs/master
<!-- [link-pipelines]: https://gitlab.com/arrowphp/arrow/pipelines -->
<!-- [link-scrutinizer]: https://scrutinizer-ci.com/g/:vendor/:package_name/code-structure -->
<!-- [link-code-quality]: https://scrutinizer-ci.com/g/:vendor/:package_name -->
