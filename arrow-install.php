<?php

declare(strict_types=1);

use Arrow\ApplicationBuilderOptions;
use Arrow\Builder\ModulesBuilderOptions;
use Arrow\CLI\Lint\LintModule;
use Arrow\CLI\Test\TestModule;
use Arrow\Interface\Module;

return new class implements Module {
	public function register(ApplicationBuilderOptions $options): void {
		$options->modules(function (ModulesBuilderOptions $options) {
			$options->registerModule(TestModule::class);
			$options->registerModule(LintModule::class);
		});
	}
};
