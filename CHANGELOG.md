# Changelog

All notable changes to `arrowphp/cli` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [0.2.20191024][0.2.20191024] - 2019-10-24

### Added
- Updated reporting flag on test with `--testdox` argument
- Updated to phpunit/phpunit v8, and phpunit/php-code-coverage ^7, and squizlabs/php_codesniffer ^3

### Fixed
- Linting

## [0.1.20190203][0.1.20190203] - 2019-02-03

### Added
- CI which runs linting.

### Fixed
- Formatting.
- Documentation.

## 0.1.20180815 - 2018-08-15

*Setup working project; refactored current CLI component into own repository.*

[0.1.20190203]: https://gitlab.com/arrowphp/cli/compare/v0.1.20180815...v0.1.20190203

<!-- 

Template - see: https://keepachangelog.com/en/1.0.0/

## [0.20180000] - 2018-00-00

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

-->
